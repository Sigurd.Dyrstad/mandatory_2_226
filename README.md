
# Information 
## Students
* Filip Raaen Mathisen 
* Sigurd Dyrstad 
* Mattias Nordahl 

## Assumptions
* By instant messaging system, we assume that it means that a message that is sent is instantly accessible by the sender and receiver. 
* We assume that the website uses HTTPS rather than HTTP. This means that we assume a secure transport of data, as is important when sending usernames and passwords.

## Important
* You have to use Google Chrome to avoid problems with the CSRF-token (Which we have experienced using Safari)

# Part 2B

## 2B.1 overview of design considerations and theory
This section will contemplate what's wrong with the structure of the initial code, as well as how this have a security impact. 

To begin with the code misses a logout button. this makes it impossible for a user to get back to the login page without closing the browser. This is a security issue because it makes it possible for a user to stay logged in even after they have left the computer. The user could also be logged in on a public computer, and this would make it possible for someone else to access the account. 

The second thing that we noticed is the way that the login function is implemented. For now it does not require a password to be entered. This is a security issue because anyone can access a user, as long as they know their username. 

Further on we have investigated whether the application is vulnerable to Cross-site scripting attacks, cross-site request forgery, SQL injection, and insecure design. We will start by briefly describing the theory behind these attacks, and then we will explain how we have tested for them.

* Cross-site scripting attacks (XSS)

    In XSS attacks, malicious scripts are injected into otherwise benign, trusted websites. By using a web application to send malicious code, generally as a browser side script, to a different end user, XSS attacks occur. It is quite common for web applications to use user input within their output without validating or encoding it, which allows these attacks to succeed.

* Cross-site request forgery (CSRF)

    CSRF attacks occur when malicious web sites, emails, blogs, instant messages, or programs cause a user's browser to perform an unwanted action on a trusted website when the user is authenticated. Cookies, including session cookies, are automatically included in browser requests when a CSRF attack is launched. As a result, if the user is authenticated to the site, it cannot distinguish between legitimate authorized requests and forged authenticated requests. This can be used to perform actions on behalf of the user, such as changing a password, purchasing items, or transferring funds.

    App.py creates a CSRF token when a user logs in. The token is needed to log in, and of we remove it we get the message shown in "Bad Request.png".  The problem here is that the same CSRF token is called for every user. In addition we need to call a CSRF token more often, ex for sending messages. This is solved by making a global function for CSRF token. 
    
* SQL injection

    SQL injection is one of the most common web application attacks. It allows an attacker to interfere with the queries that an application makes to its database. It is the placement of malicious code in SQL statements, via web input.

    In app.py there exists a database for handling messages and announcements. This database i vulnerable for SQL injections. We could write a message that would delete the entire database. An example of this is if we write the message: A message...'); DROP TABLE messages; print('h . This will delete the table messages. See the screenshot attached "SQL injection.png". This could be prevented by prepared statements (see next part). 

* Insecure design

    The concept of insecure design incorporates a variety of risks that can be incurred from ignoring architectural and design best practices during the planning phase and before implementation. Note that an insecure design differs from an insecure implementation, and that a near-perfect implementation cannot prevent defects arising from an insecure design. Insecure design is a risk that can be mitigated by following best practices and by performing a security design review.

* Sniffing

    Sniffing, also known as network analyzers, is a piece of software that can capture and log the packets that travel over a network. A packet sniffer is a tool that is mostly common for legitimate use, but can also be abused in the form of sniffing attacks. Examples of packet sniffing software are: Wireshark, TCPDUMP and SolarWinds Network Packet Sniffer. 

    The initial application shows vulnerability to sniffing attack. We discovered this while pen testing the application. In the next part we will describe in detail how we secure the website against sniffing attacks. 

* Clickjacking

    When an attacker tricks a user into clicking on a button or link on a different page when he or she was intending to click on the top level page, this is known as clickjacking. The attacker is "hijacking" clicks intended for their page and redirecting them to another page, probably owned by another application. 

    As with the sniffing attack, we discovered that the initial application is vulnerable to clickjacking throug pen testeing. Also this is now secured and described in the part below. 


Comment: 
We assume that the appliaction uses https. However, this is difficult to implement in this task as it requires authentication. Therefore, we assume that the security flaws related to us using http is not relevant for this task.

## 2B.2 Features of our applications and technical details

* Logout button:

    We created a logout logout button using the Flask package. It creates a new page "/logout" that log outs the current user, and redirects to the "/login"-page. This method requires that the user is logged in when called.
  
* Database of users:

    Instead of having our users saved as a dictionary we created a new table in the tiny.db database that stores users. This table stores the user's username and hashed password. We did so by creating a new table in the tiny.db database. Furthermore, we inserted our predefined users into the database. The reason for making a database it to persist the users, so that they are not lost when the application is restarted.

* Create new user

    In the initial program, it was not possible to create new users. We wanted to solve this issue to improve the user experience and functionality. However, the most important reason is because we want to hash the passwords. To show that we are able to do this in the correct manner, we will have to include functionality for registering new user. We will come back to this. The following will explain how we added the functionality to register the new users. 

    At this point we had fixed the SQL databe to include a users table. We therefore decided to utilize this. The first step was to create a register page. We used a design similar to the login page. In addition we added buttons to the register page from the login page, and from the register page, back to the login page. Then we created a form using FlaskForm.

    In app we have to create a method to insert the registered passwords from the form into our SQL database. This happens when the form is submitted. Then the page is rendered again, and the password is updated. The user can then go back to the login page and log in with the new password. 

    In order for the registration to be successfull the username needs to be unique, and the password needs to meet certain requirements (More on this below).

* Password
    We made it so the user has to type the correct password in order to log in. The program hashes the password inserted by the user, and checks whether it can find a entry in the users database that matches both the username and the hashed password. If it does, the user is logged in, else the user is denied access.

* Minimum length password
    In order to force the user to select safe passwords, the program requires that the password is atleast 8 charackers long. It does so in the registration of a new user, where it refuses to insert a new row to the users database if the password does not meet the requirements.

* Hashing and salting the passwords

    We have chosen to both hash and salt the passwords in our database. The reason for combining hash and salt is that when the random characters are added to passwords prior to hashing, the hacker loses the ability to quickly figure out the plaintext password.

    Whenever a user attempts to login or register a new user, the password is sent to the server as plaintext. We decided that it is unecessary to hash the password client-side since it will not make it more secure. The reason is the following:

    Hashing client-side
    If the password is hashed client-side, the username and password will be sent togheter (the username in plaintext, and the password hashed). However, once the information reaches the server, the program simply checks whether the hashed value is the same as the hashed value stored in the database. This means that a listening hacker simply can copy the username and hashed password, and gain access (since the password is not hashed server-side)

    Hashing server-side
    If the password is hashed server-side, the username and password are both sent in plaintext. Once the password reaches the server, it is hashed and checked against the hashed password in the database. A listening hacker can retrieve the plaintext username and password, and use it to login.

    Thus, it makes no difference whether the password is hashed client-side or server-side. To secure the login- and registration process we need to have a secure channel of datastream (Which can be provided by for instance RSA Encryption)
    Thus, we simply hash our passwords server-side, using salts to avoid the hacking tecnique "rainbow table". The salts are stored with the password hash, separated by a ":". A rainbow table is an efficient way to store data that has been computed in advance to facilitate the cracking of hashed data. The rainbow table is a list of precomputed hashes, which can be used to quickly find the original plaintext password. The table is generated by hashing every possible password, and storing the result. The table is then sorted by the hash, so that it can be searched quickly. The table is then stored on the attacker's computer, and can be used to crack any password that is hashed using the same algorithm. The attacker can then use the table to find the original plaintext password, and use it to login.

* Restricted from-sender

    Initially, the user could choose both who the sender is, and who the recipient is. This is an obvious security-problem. To solve this, we used the Flask package to retrieve the username of the current user (flask_login.current_user.id). Thereafter, this username is automatically used as the sender. Thus, it is not possible for the user to choose who the sender of the message is, since it will automatically be himself.

* Restriction to viewing messages

    When a user requests the all messages, the SQL statement specifies that it should only return the messages in which the user is either the sender or the recipient, using WHERE in the SQL-query. The username of the current user is retrieved with the Flask package - ensuring that only the chats of the logged in user is shown. This is a security measure that prevents a user from seeing messages that they are not supposed to see.

* Extension of messages
    We extended the properties of messages to include recipient, timestamp and a replyID. The recipient is retrieved from the "To"-field. The timestamp is automatically set when the SQL insertions is performed. The reply ID is chosen as follows:
    If the recipient of the message has already sent a message to the sender, the ID of the last message the recipient sent to the sender is selected as the replyID. If the recipient has never sent a message to the sender, the replyID will be 0. This offers the added feature where it is possible to derive all non-reply messages.

* Blocking/Unblocking of users
    We added features that allows the user to block and unblock other users. If you attempt to send a message to a user that you have blocked, or who have blocked you, the message will not be sent. An user can also unblock a user which he has previously blocked. 

    We implemented this by creating a new table in the tiny.db database named "block". This table consists of a blockID, blocker and blocked. Thus, whenever a person blocks a user, a new entry is created in this table. Whenever a user unblocks a user, the entry is deleted from the database table.

    The block table has a contstraint that ensures that all entries are unique. This is to prevent one person to block another person several times at a given moment.

* SQL Injection
    We have prevented the user from performing SQL-injections by introducing prepared statements. This is done through the use of parameterized queries. These parameterized queries forces the developer to first define all the SQL code, then pass the parameters later. This allows the database to distinguish between code and data, regardless of what the user supplies.

    Example:
    stmt = f"SELECT * FROM messages WHERE message GLOB ? AND (sender =  ? OR receiver = ?)"
    val = (query, user, user)
    database.sql_query1(stmt, val)

* Preventig Cross Site Request Forgery (CSRF)

    As mentioned, the login page is protected against CSRF. When we made the registration page we have used the same measures to prevent CSRF attacks. We created a form using flask forms and {{ form.csrf_token }} to generate a CSRF token. This token is then sent to the server, and the server checks whether the token is valid. If the token is valid, the user is registered. If the token is invalid, the user is not registered. 

    We have also enabeled global CSRF protection for our app. This is done by importing flask_wtf.csrf and adding the this line to the app.py file: 
    csrf = CSRFProtect(app)
    We have also created a new secret key to sign the token. Our key is created using the os package.

    We had to exempt the block, unblock and send function from the CSRF token to make the app work. We believe this might be due to the app using http instead of https.
    

* Preventing XSS

    We have partly prevented XSS attacks by manually escaping inputs through the escapeOutput()-function. Ideally, we would have managed to do this using a package, but we were unable to do so. We know we are discouraged to use such manual techniques, but we decided to keep it as it should provide more protection than if we were to remove it.

    Additionally, our X-Content-Type-Options command early in the app.py-file forces the browser to honor the response content type instead of trying to detect it, which can be abused to generate a XSS attack. Thus, the "nosniff" helps avoid XSS attacks.

* Cookie options

    We have altered the cookies to make them more secure. The options we have altered are: 
    - SESSION_COOKIE_SECURE=True,
    - SESSION_COOKIE_HTTPONLY=True,
    - SESSION_COOKIE_SAMESITE='Lax'

With secure we limit the traffic to HTTPS traffic. 
With HTTPONLY we prevent the cookie from being accessed by JavaScript.
With SEMESITE set to "Lax" we prevents sending cookies on cross-site requests, situations where we are voulnerable to CSRF attacks. However, cookies are still sent when a user is navigating to the original site. 
   
* Preventing sniffing

    We have used the header X-Content-Type-Options to "nosniff". Doing this prevents content or MIME sniffing. This option tells browsers to use the server provided Content-type and not interpret the content as a different type. This can be abuse to perform a XSS attack, and is therefore important to prevent. 

* Preventing clickjacking

    We have prevented the possibility of clickjacking by using the header X-Frame-Options. This header prevents external pages to iframe our site. Therefore they cannot do an attack where clicks on the outer frame can be translated to clicks on our pages elemets. 


* Refactoring
    
    We have done some refactoring of the code. This is done to improve the design, structure and implementation of the software. It improves the readability and reduces complexity. In addition, it makes it easier for us to find bugs and vulnerabilities. 

    * Put both the forms, login and registration, in a folder called forms. 
    * Placed the graphics in a grapich folder. 
    * Taken some of the functionality from app.py into each own files that serve a specific puprose. These files are put in the 
        * csrf.py - A file generating an csrf token.
        * database.py - A file that handles the connection to the database, the query and adding the initial data. 
        * hash.py - A file that hashes a password and adds salt. 
        * pygmentize.py - A file that pygmentize the code.


## 2B.3 Instructions on how to use the application
___
Important! Use Google Chrome!
___

### General functionalities: 
1. To use and test the application, open the project, and in the terminal write "flask run". Then open the url that is displayed in the terminal in Google Chrome. 

2. The first page you will see is the login page. There you find two buttons. Login and register. If you press register, you are taken to a register page. Here you can write a new username and password. The password has to be of a certain length, and the username must not be taken. If that is the case, you will recieve a message that the registration was successfull. 

3. Back at the login page you can type in the new username and password. You will then go to the main page. Here you have different options. To send a message you can click on the "Send" button. First you have to write the name of the person you want to send to, and the message in the message box. After this is done you can click "Send". 

4. To view the messages you have recieved and sent you can either use the "Search!" button or the "Show all" button. The "Show All" button lets you see all the messages you have received and sent. To see a specific message, you have to write the mesage in the search field and clik the "Search!" button.

5. We have also implemented functionality for blocking and unblocking users. To block a user, write the name of the user you want to block in the "To"-field. Now you cannot send message to that user, and that user cannot send messages to you. 
Thereafter you can unblock the same user, and try to send a message - and it will work. 
You can also log into the blocked user to try and send a message to the user that blocked it, and it will not be able to send a message, nor unblock itself.

6. Lastly, to log out, you can click the "Log out" button. This will take you back to the login page.

To simulate a chat, you can log in as user x and send a message to user y. Then you can login as user y and view the message.  

### Attack testing:

#### Manual SQL Injection 

Write "A message...'); DROP TABLE messages; print('h" in the message field, and click send. In the initial code this leads to the program dropping the message table. In our progam however, it does not do so due to our prepared statements.

#### Automated testing
Use OWASP ZAP to attack our website.
This should only return a single security error (if we neglect the ones due to the site not being HTTPS). This error is due to our website partly being exposed to XSS attacks. 

It should however be secured against the other attacks described in this assignment.


## 2B.4 questions and answers

 ### Q1: Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

Answer: This question is answered based on how the application was configured before we started fixing it. 

Anyone who has the skillset to do so can attack the application. As shown above you do not need to much hacking experience to example delete the database. The attacker can do anything that the user can do. This includes sending messages, deleting messages, and reading messages. The attacker can also create new users, and delete users. The attacker can also delete the entire database. 

* Confidentiality

    Confidentilaity is the protection of information against unauthorized disclosure. The attacker can read all messages, and therefore gain access to confidential information.

* Integrity

    Integrity is the protection of information against unauthorized modification. The attacker can modify messages, and therefore modify the integrity of the information.

* Availability

    Availability assures that systems work promptly, and services is not denied to authorized users. The attacker can delete messages, and therefore deny the availability of the service.

In the initial solution there is almost no limit to what a hacker can do. The only limit is that the hacker needs to have the skillset to do so. 



### Q2: What are the main attack vectors for the application?

Answer: Firstly, we define what a an attack vector is. An attack vector is a method of gaining unauthorized access to a network or computer system

The main attack vectors for the initial application is:
* SQL injection
* Cross-site scripting attacks (XSS)
* Cross-site request forgery (CSRF)
* Insecure design
* Clickjacking
* Sniffing

For detailed descriptions of these attacks, and how the application are vulnurable to these, please refer to part 2B.1.


### Q3: What should we do (or what have you done) to protect against attacks?

Answer: There are several things that should be done (and have been done in this application) to protect against attacks. This answer will shortly summarize the most important ones. To se in detail what we have done, please see part 2B.2.
* Use prepared statements on databases
* Use a database to store users
* Prevent users from sending messages from other accounts
* Hash and salt passwords 
* XSS
* CSRF
* Insecure design

### Q4: What is the access control model?

Answer: Access control is a security technique that regulate who or what can view or use resources in a computing enviornment. In our application we have implemented a mandatory access control model (MAC). This means that the access control is enforced by the application. No end user can access a resource that they are not supposed to access, or can control any settings that provide and pvilieges to anyone. The user in our application can only access their own profile and their own messages. 

### Q5: How can you know that you security is good enough?

Answer: The most common way to test if the security is good enough is to perform a penetration test (pen test). This is an authorized simulated attack performed on a computer system to evaluate its security. The purpose of a pen test is to find vulnerabilities that an attacker could exploit. An exemple of a pen test is where a professional hacker tries to break into the application. If the hacker is not able to break into the application, the security is good enough. There also exists several tools to perfom pen tests, such as OWASP ZAP.

However, it is worth noting that those who prevent attacks are always one step behind those who attack. By this, we mean that the attackers continously create new ways to attack. Thus, one could argue it is dangerous to deem their application's security as good enough, as one should always be on the lookout for weaknesses.

