from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from forms.register_form import RegisterForm
from forms.login_form import LoginForm
from json import dumps
from base64 import b64decode
from apsw import Error
from markupsafe import escape
from utils import database, pygmentize, hash
import os
from flask_wtf.csrf import CSRFProtect


# Set up app
app = Flask(__name__)
app.config.update(
    DEBUG=True,
    SECRET_KEY= os.urandom(24).hex(),
    SESSION_COOKIE_SECURE=True,
    SESSION_COOKIE_HTTPONLY=True,
    SESSION_COOKIE_SAMESITE='Lax'
)

# This prevents CSRF attacks
csrf  = CSRFProtect(app)
csrf.init_app(app)

# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass

@app.after_request
# Prevent the browser from trying to honer the response type. Prevents MIME confusion attacks and Unauthorized hotlinking
# Prevents clickjacking by preventing external sites from embedding our site in an iframe
def add_header(response):
    response.headers['X-Content-Type-Options'] = 'nosniff'
    response.headers['X-Frame-Options'] = 'SAMEORIGIN'
    #response.headers['X-XSS-Protection'] = '1; mode=block'
    #response.headers['Content-Security-Policy'] = "default-src 'self'"
    return response

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    
  #  stmt = f"SELECT * FROM users WHERE username = '{user_id}'"
    stmt = f"SELECT * FROM users WHERE username = ?"
    val = {user_id}
    q = database.sql_query1(stmt, val)
    if not q:
        return
    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user
login_manager.login_message = u""

# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')

        #stmt = f"SELECT password FROM users WHERE uid = '{uid}'"
        stmt = f"SELECT password FROM users WHERE uid = ?"
        val = {uid}

        u = database.sql_query1(stmt, val)

        if u and u == passwd: # and check_password(u.password, passwd):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        for uid in users:
            if users[uid].get('token') == auth_params:
                return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')

@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')

# This method is used to verify a user whenever he tries to login. 
# It received the username and password in plaintext. Thereafter, it 
# hashes the password, and checks whether the database table "users"
# has any entries with both the username and hashed password togheter.
# If it does, it logs in, otherwise it refuses the user.
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username_cur = form.username.data # Plaintext username
        password = form.password.data # Plaintext password
        
        stmt = f"SELECT * FROM users WHERE username = ?"
        val = {username_cur}
        q = database.sql_query1(stmt, val) # Checks if the username is stored in the database

        if q:
            u = q[0][0] #Retrieves username
        else: 
            return render_template('./login.html', form=form)
        correctPassword = q[0][1] # Hashed correct password, stored in database
  

        if u and hash.matchHashedText(correctPassword, password): # Checks whether the entered password is correct
            user = user_loader(username_cur)
            # automatically sets logged in session cookie
            login_user(user)

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
    return render_template('./login.html', form=form)

# This method logs the user out, and redirects the website to the login site.
@app.route('/logout')
@login_required
def logout():
    flask_login.logout_user() # They will be logged out, and any cookies for their session will be cleaned up.
    return flask.redirect("/login")

# This method searches for messages in the database
# If the user does not specify a specific id, the method retrieves all messages where
# the user is either the sender or recipient
@app.get('/search')
def search():
    user = flask_login.current_user.id
    query = request.args.get('q') or request.form.get('q') or '*'
    
    if (query == '*'):
        stmt = f"SELECT * FROM messages WHERE (sender =  ? OR receiver = ?)"
        val = (user, user)
    else:
        stmt = f"SELECT * FROM messages WHERE id = ? AND (sender =  ? OR receiver = ?)"
        val = (query, user, user)
    result = f"Query: {pygmentize.pygmentize(stmt)}\n"
    rows = database.sql_query1(stmt, val) # Only retrieves messages in which the user is the sender or recipient

    result = result + 'Result:\n'
    for row in rows: # Creates a list of the query output
        result = f'{result}    {dumps(row)}\n'
    return result

# This method allows for users to block other users by inserting a row
# into the block table in the database.
@app.route('/block', methods=['POST', 'GET']) 
@csrf.exempt
def block():
    try:
        blocker = flask_login.current_user.id #MAKING IT IMPOSSIBLE FOR PEOPLE TO SEND FROM OTHER PEOPLES ACCOUNT
        blocked = request.args.get('receiver2') or request.form.get('receiver2')

        if not blocked or not blocker:
            return f'ERROR: missing blocked or blocker'
        
        stmt = f"SELECT * FROM block WHERE (blocker = ? AND blocked = ?)"
        val = (blocker, blocked)
        q = database.sql_query1(stmt, val) # Checks whether the user already has blocked the person
        if q:
            return f'ERROR: You have already blocked that user'

        stmt = f"INSERT INTO block (blocker, blocked) VALUES (?, ?);"
        val = (blocker, blocked)
        database.sql_query1(stmt, val) # Blocks the person
        result = f"Query: {pygmentize.pygmentize(stmt)}\n"
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'

# This method is called whenever a user clicks the unblock-button.
# It checks whether the user has blocked the person he attempts to unblock.
# If he has, the block is removed.
@app.route('/unblock', methods=['POST', 'GET'])
@csrf.exempt
def unblock():
    try:
        unblocker = flask_login.current_user.id # Get the user that is logged in
        blocked = request.args.get('receiver2') or request.form.get('receiver2')# Get the user to be unblocked

        if not blocked or not unblocker:
            return f'ERROR: Choose what user to block in the "To:-field"'

        stmt = f"SELECT * FROM block WHERE (blocker = ? AND blocked = ?)"
        val = (unblocker, blocked)
        q = database.sql_query1(stmt, val) 
        if(q): # Checks whether the user has blocked the person 
            stmt = f"DELETE FROM block WHERE (blocker = ? AND blocked = ?)"
            database.sql_query1(stmt, val) # Unblocks
            result = f"Query: {pygmentize.pygmentize(stmt)}\n"
            return f'{result}ok'
        if(not q): 
            return f"You have not blocked that user"
    except Error as e:
        return f'{result}ERROR: {e}'

# This method sends a message to the recipient if the recipient has not blocked the sender 
# and the sender has not blocked the recipient
@app.route('/send', methods=['POST']) # FJERNET 'GET'
@csrf.exempt
def send():
    try:
        sender = flask_login.current_user.id # Get the user that is logged in
        receiver = request.args.get('receiver') or request.form.get('receiver') 
        message = request.args.get('message') or request.args.get('message')
        
        if not receiver or not sender or not message:
            return f'ERROR: missing receiver, sender or message'

        stmt = f"SELECT * FROM block WHERE (blocker = ? AND blocked = ?) OR (blocker = ? AND blocked = ?)"
        val = (sender, receiver, receiver, sender)
        q = database.sql_query1(stmt, val) 
        if q: # Checks whether the user is blocked from sending messages to the recipient, or whether the sender has blocked the recipient
            return f"Error: You have blocked, or been blocked by that user"

        stmt = f"SELECT username FROM users WHERE username = ?;" 
        val = {receiver}
        q = database.sql_query1(stmt, val)

        if not q: #Checks if the recipient is a valid user
            return f'Invalid recipient' 

        stmt = f"SELECT id FROM messages WHERE (sender = ?) ORDER BY id DESC LIMIT 1"
        val = (receiver,)
        reply = database.sql_query1(stmt, val) # Checks whether the user has previously received a message from the recipient

        if not reply:
            reply = 0 # If the user has not recieved a message from the recipient previously, the replyID is 0
        else:
            reply = reply[0][0] #Retrieves the ID of the previous message sent to the user from the recipient
        
        stmt = f"INSERT INTO messages (receiver, sender, message, reply) values (?, ?, ?, ?);"
        val = (receiver, sender, message, reply)
        result = f"Query: {pygmentize.pygmentize(stmt)}\n"
        database.sql_query1(stmt, val) # Send/Store the message
        
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'

@app.get('/users')
def user():
    stmt = f"SELECT username, password FROM users;"
    c = database.sql_query1(stmt, None)
    anns = []
    for row in c:
        anns.append({'username':escape(row[0]), 'password':escape(row[1])})
    return {'data':anns}


@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(pygmentize.cssData)
    resp.content_type = 'text/css'
    return resp

# This method allows for a new user to create an account.
# It checks whether the username is taken, and whether the password is of valid length.
# If they are, it inserts a new user to the users table on the database.
@app.route('/register', methods=['GET','POST'])
def register():
    form = RegisterForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username_cur = form.username.data
        password = hash.hashText(form.password.data)
        
        stmt = f"SELECT * FROM users WHERE username = ?"
        val = (username_cur,)
        q = database.sql_query1(stmt, val) 

        if (len(form.password.data) >= 8 and not q): # Checks whether the password is long enough and whether the username is already taken
            stmt = f"INSERT INTO users (username, password) VALUES (?,?);"
            val = (username_cur, password)
            database.sql_query1(stmt, val) # Register new user
            flask.flash('Registration was succesful')
        else:
            if(len(form.password.data) < 8):
                flask.flash('Password needs to be atleast 8 characters') # Informs the user
            if(q):
                flask.flash("That username is taken") # Informs the user
 
    return render_template('./register.html', form=form)


# We believe the code below is redundant, but kept it just in case

@app.get('/announcements')
def announcements():
    stmt = f"SELECT author,text FROM announcements;"
    c = database.sql_query1(stmt, None)
    anns = []
    for row in c:
        anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
    return {'data':anns}

@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"
