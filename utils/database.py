import sqlite3
import sys
import apsw
from apsw import Error

conn = None

def sql_query1(stmt, vals):
    try:
        c = conn.execute(stmt, vals)
        q = c.fetchall()
        return q
    except Error as e:
        return (f'ERROR: {e}', 500)

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS block (
         blockID integer PRIMARY KEY,
         blocker TEXT NOT NULL,
         blocked TEXT NOT NULL,
         CONSTRAINT uniq UNIQUE (blocker, blocked)
         );''')
    c.execute('''CREATE TABLE IF NOT EXISTS users (
         username TEXT PRIMARY key,
         password TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        receiver TEXT NOT NULL, 
        message TEXT NOT NULL,
        reply integer,
        times DATETIME DEFAULT CURRENT_TIMESTAMP);''') #ADDED TIMESTAMP, RECIPIENT AND REPLY
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')

    ### ADD PREDEFINED USERS
    u = [('alice','b738a65c6cd622058eac5e4dfdf3547e7f67a7c17095bc4d804483a5b0b827e0:ff99388d2338498a9cec815b32f8f35d', 2), 
         ('bob', '00219781b708043abbb03c081132dfe27bdab7eee814d5f5dcac66492cd5b81e:f9fc03230f75443e98d5c21713d16ea3', 3),
         ('token', '2ef5c7a2f6d3e60471e419dc42eb77922ebbfa7243821d70d65e2f4f4fb45375:72f7d6b3d1584c39afb9b6cc094a937d', 4)]
    for user in u:
        stmt = f"SELECT username FROM users WHERE username = ?"
        q = sql_query1(stmt, {user[0]})
        if not q:
            username_cur = user[0]
            password = user[1]
            stmt = f"INSERT INTO users (username, password) VALUES (?,?);"
            val = (username_cur, password)
            sql_query1(stmt, val)
except Error as e:
    sys.exit(1)

